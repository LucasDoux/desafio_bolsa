exports.up = function(knex) {
    return knex.schema.createTable('anotations',function(table){ //criando entidades
        table.increments(); //criando chave primaria auto incremental
        
        table.string('title').notNullable(); // criando atributo nome, não nulo
        table.string('description').notNullable();
        table.decimal('value').notNullable();

        table.string('user_id').notNullable(); 

        table.foreign('user_id').references('id').inTable('users'); //chave  estrangeira, que referencia 'id' na tabela 'users'
      });
};

exports.down = function(knex) {
    return knex.schema.droTable('anotations');
};
