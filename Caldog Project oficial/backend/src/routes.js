const express = require('express'); //importando

const UserController = require('./controllers/UserController'); //importando

const AnotationController = require('./controllers/AnotationController'); //importando

const ProfileController = require('./controllers/ProfileController'); //importando

const SessionController = require('./controllers/SessionController');
const { request, response } = require('express');



const routes = express.Router(); //desacoplando o modulo de rotas do express em uma nova variavel

routes.post('/sessions', SessionController.create); //criando uma sessão(login)


routes.get('/users', UserController.index);//usa-se index para listar todos os dados de uma tabela
routes.post('/users', UserController.create); //criando a primeira rota e passando uma função como segundo parametro

routes.get('/profile', ProfileController.index);
   
routes.get('/anotations', AnotationController.index);    
routes.post('/anotations', AnotationController.create); 
routes.delete('/anotations/:id', AnotationController.delete) //rota para deletar um caso de incidents

module.exports = routes; //exportando as rotas