const crypto = require('crypto');
const connection = require('../database/connection');



module.exports = {  //exportando objeto
    async index (request,response) { //faz listagens os dados da tabela

        const users = await connection ('users').select('*'); //acessando todos os registros da tabela users
    
        return response.json(users);
    
    },

    async create(request,response){
    const {name , email , dog , city , uf}  = request.body; //fazendo a desestruturação e alocando as infos em cada variavel necessarias acessando o corpo da requisição
    
    const id = crypto.randomBytes(4).toString('HEX'); //gerando 4 bytes de caracteres decimais aleatorios

   await connection('users').insert({ //inserindo dados na tabela 'users'
        id,
        name,
        email,
        dog,
        city,
        uf,


    });

    return response.json({ id }); //acessa as respostas e Retorna aos usuarios
    }
};