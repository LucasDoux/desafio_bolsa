const connection = require('../database/connection');

//rota responsavel por verificar se um usuario existe ou não
module.exports = {
    async create(request, response){ 
        const{ id } = request.body;//buscando id atraves do corpo da requisição


        const user = await connection('users') 
        .where('id', id)//buscando um user
        .select('name')
        .first();


        if(!user) { //se esse user nao existir
            
        return response.status(400).json({
        error: 'No USER found with this ID'
        });
    }
    return response.json(user)//retornando os dados do user
}
}