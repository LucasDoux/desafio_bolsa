const connection = require('../database/connection');

module.exports = { //exportando objeto
    
    async index(request,response){ //listar todos os dados das anotções

        const { page = 1 } = request.query;//buscando parametro page dentro do request 

        const [count] = await connection('anotations').count(); //query a parte, retorna a quantidade de anotações totais
        

        const anotations = await connection('anotations') //acessando todos os registros da tabela Anotações
        .join('users', 'users.id', '=' , 'anotations.user_id')//relacionando dados de tabelas
        .limit(5) //limitando a busca de dados
        .offset((page - 1) * 5) //apenas lista 5 anotações por vez
        .select(['anotations.*',
         'users.name',
         'users.email',
         'users.dog',
         'users.city', 
         'users.uf']);

        response.header('X-Total-Count',count['count(*)']) //acessando as anotações atraves do cabeçalho e se comunicando com o frontend

        return response.json(anotations);
    },

   

    async create(request,response){ //criando um anotations
        const { title , description , value} = request.body; // //fazendo a desestruturação e alocando as infos em cada variavel necessariaZ acessando o corpo da requisição
        const user_id = request.headers.authorization; //pegando o id do usuario

        const [id] = await connection('anotations').insert({//pegando o id | inserindo dados na tabela de 'anotations'
            title,
            description,
            value,
            user_id,
        });

        return response.json({ id }); //retornando o id do usuario
         
       // request.headers; //cabeçalho da requisição, guarda informações do contexto da requisição
    },
    
    async delete(request,response){
        const { id } = request.params;  //pegando o id do parametro de rota
        const user_id = request.headers.authorization;  //pegando o id do usuario Logado

        const anotation = await connection('anotations')  //buscando anotation dentro da tabela anotations
            .where('id',id)     //buscando anotation especifico
            .select('user_id')  //selecionando apenas a coluna 'user_id'
            .first();         //retorna apenas 1 resultado

            if(anotation.user_id != user_id){ // se o id do anotation do usuario for diferente do usuario id esta logado
                return response.status(401).json({ //erro de autorização padrao
                    error: 'Operation Not Permitted.'
                });
            }

            await connection('anotations').where('id',id).delete(); ///deleta o registro de dentro da tabela de dados

            return response.status(204).send(); //Status succeed 200-299 retorna uma resposta para o front-end que não tem conteudo
    }

};