const connection = require('../database/connection');

//responsavel por retornar as anotations especificas de um unico usuario
module.exports = {

    async index(request, response) {
     const user_id =  request.headers.authorization;   //acessando os dados do user logada
      
     const anotations = await connection('anotations')  //buscando todos as anotations desse user que ela criou
     .where ('user_id', user_id)
     .select('*'); //buscando todo os campos dessas anotations

     return response.json(anotations);
    }
}