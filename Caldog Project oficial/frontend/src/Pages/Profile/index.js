import React, {useState,useEffect} from 'react'; //useEffect: serve para disparar alguma função em um determinado momento do componente
import { Link, useHistory } from 'react-router-dom';
import { FiPower, FiTrash2 } from 'react-icons/fi';

import api from '../../Services/api';

import './styles.css';

import avatarDog from '../../Assets/dog.png';

export default function Profile(){

    const [anotations, setAnotations] = useState([]); //começa vazio

    const history = useHistory();
    const userId = localStorage.getItem('userId'); //salvos no local storage
    const userName = localStorage.getItem('userName');  //salvos no local storage

    useEffect(() => { //dispara uma função assim que o componente é mostrado em tela
        api.get('profile',{
            headers: {
                Authorization: userId,
            }
        }).then(response => {
            setAnotations(response.data);
        })
    }, [userId]); //array de dependencia, assim que as informações dentro desse array mudarem, ele executa a funçã acima novamente



    async function handleDeleteAnotation(id){
        try{

            await api.delete(`anotations/${id}`, {
                headers: {
                    Authorization: userId,
                }
            });


            setAnotations(anotations.filter(anotation => anotation.id !== id)); //ao apagar , some instantaneamente da tela; mantem os incidents que são diferetens daquele apagados
        
        }catch(err){
            alert('Erro ao deletar caso, tende novamente.');
        }
    }




    function handleLogout(){
        localStorage.clear(); //limpando o local storage
        
        history.push('/') //enviando para a rota raiz(logon)
    }



    return(
        <div className="profile-container">
            
            <header>
                
                <img src={avatarDog} alt="Be The Hero"/>
                <span>Bem Vindo, <b>{userName}</b> </span>

                <Link className= "button" to = "/anotations/new">Adicionar novo produto ✚</Link>

                <button onClick= {handleLogout} type="button">
                    <FiPower size={24} color='#E02041'/>
                </button>

            </header>


            <h1>Suas Anotações Abaixo:</h1>

            <ul>
                  {/*codigo em JS, percorrendo cada um dos incidentes e retornando um conteudo JSX*/ }

               {anotations.map(anotation => (
                    <li key ={anotation.id}>
                    <strong>PRODUTO:</strong>
                    <p>{anotation.title}</p>

                    <strong>DESCRIÇÃO:</strong>
                    <p>{anotation.description}</p>

                    <strong>VALOR:</strong>
                    <p>{Intl.NumberFormat('pt-BR', { style: 'currency' , currency: 'BRL'}).format(anotation.value)}</p> 
                    {/*INTL: internacionalização de valores de dados*/ }
                    {/*passando o idioma utilizado, passando o formato do numero(no caso é moeda), passando qual a moeda*/ }

                    <button onClick={() => handleDeleteAnotation(anotation.id)} type="button">
                        <FiTrash2 size={20} color="1C1C1C"/>
                    </button>
                </li>
               ))}           

            </ul>

           

        </div>
    );
}