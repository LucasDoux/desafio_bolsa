import React, {useState} from 'react';
import {Link, useHistory} from 'react-router-dom' //Componente link para acessar rotas de maneira rapida e sem recarregar uma pagina inteira
import {FiLogIn} from 'react-icons/fi' //Necessita de instalação npm install react-icons ICONES


import api from '../../Services/api'

import './styles.css'

import logoImg from '../../Assets/Logo.png';
import humanDog from '../../Assets/walking dog.png';


export default function Logon(){
    const [id,setId] = useState('');
    const history = useHistory();//server para fazer uma navegação atraves de uma função javascript, quando não se poder usar o link

    async  function handleLogin(e){
        e.preventDefault();


        try{
            const response = await api.post('sessions', { id });

            localStorage.setItem('userId', id);
            localStorage.setItem('userName',response.data.name);

            history.push('/profile');
        }catch(err){
            alert("Falha ao logar, tente novamente");
        }
    }

    return(
        <div className ='logon-container'>

            <section className="form">

                <img src={logoImg} className="logoImg" alt="CalDog"/>

                <form onSubmit={handleLogin}>

                    <h1>Faça seu login</h1>

                    <input placeholder= "Qual Sua ID?"
                        value={id}
                        onChange={e => setId(e.target.value)}
                    />
                    <button className="button" type="submit">L O G A R</button>

                    <Link className="back-link" to="/register">
                        <FiLogIn size={24} color="#E02041"/> 
                        Não tenho cadastro
                    </Link>

                </form>

            </section>

            <img src={humanDog} className="ilustration" alt="Heroes"/>
        </div>
    );
}