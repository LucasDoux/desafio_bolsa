import React, { useState} from 'react';
import { Link, useHistory } from 'react-router-dom';
import {FiArrowLeft} from 'react-icons/fi';


import api from '../../Services/api'
import './styles.css'

import logoImg from '../../Assets/Logo.png';

export default function Register(){

    const [name,setName] = useState('');
    const [email,setEmail] = useState('');
    const [dog,setDog] = useState('');
    const [city,setCity] = useState('');
    const [uf,setUf] = useState('');


    const history = useHistory();


    async function handleRegister(e){ //responsavel por fazer o cadastro do usuarios
        e.preventDefault(); //impede que a pagina seja recarregada ao executar tal ação

        const data ={
            name,
            email,
            dog,
            city,
            uf,
        };

        try{

            const response = await api.post('users', data);

            alert(`Seu ID de acesso: ${response.data.id}`);//utilizando acrase `` para colocar variaveis dentro do texto
            
            history.push('/')//enviando apos o cadastro para o login
            
        }catch(err){
            alert('Erro no cadastro, tente novamente');
        }
    } 

    return (
        <div className="register-container">
           
           <div className = "content">

                <section>
                    
                    <img src={logoImg} className="logoRegister" alt="CalDog"/>
                    <h1>Cadastro</h1>
                    <p>Faça seu cadastro, para poder realize suas anotações do seu cachorro.</p>

                    <Link className="back-link" to="/">
                        <FiArrowLeft size={24} color="#E02041"/> 
                        Voltar ao login
                    </Link>

                </section>


                <form onSubmit={handleRegister}>


                    <input placeholder= "Nome Do Responsável"
                           value={name}
                           onChange={e =>setName(e.target.value)} 
                           

                    /> 

                    {/*e.target.value = valor do input */}


                    <input type="email" placeholder="E-mail"
                            value={email}
                            onChange={e =>setEmail(e.target.value)}
                    
                    />



                    <input placeholder="Nome Do Cachorro"
                    
                            value={dog}
                            onChange={e =>setDog(e.target.value)}
                    
                    />


                    <div className="input-group">
                        

                        <input placeholder="Cidade"
                            value={city}
                            onChange={e =>setCity(e.target.value)}
                        
                        />



                        <input placeholder="UF" style = {{width: 80}}
                             value={uf}
                            onChange={e =>setUf(e.target.value)}
                        
                        
                        />

                    </div>

                    <button className="button" type = "submit"> CADASTRAR</button>

                </form>


            </div>

        </div>
    );
}