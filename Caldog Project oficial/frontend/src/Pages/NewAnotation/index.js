import React, {useState} from 'react';
import { Link, useHistory } from 'react-router-dom';
import {FiArrowLeft} from 'react-icons/fi'

import api from '../../Services/api'

import './styles.css'

import logoImg from '../../Assets/Logo.png';

export default function NewAnotation(){


    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [value, setValue] = useState('');

    const history = useHistory();

    const userId = localStorage.getItem('userId');



     async function handleNewAnotation(e){
        e.preventDefault(); //previnindo o comportaamento padrao do formulario
    


     const data = {
        title,
        description,
        value,
        };


        try{

            await api.post('anotations',data,{
                headers:
                {
                    Authorization: userId,
                }
            })

            history.push('/profile'); //movendo o usuario pra rota de profile dps de realizar o registro do caso

        }catch(err){
            alert('Erro ao cadastrar caso, tente novamente.');
        }

    }


    return(
        <div className="new-incident-container">
           
           <div className = "content">

                <section>
                    
                    <img src={logoImg} className="logoPlus" alt="Be The Hero"/>
                    <h1>Cadastrar novo produto</h1>
                    <p>Use os campos ao lado para realizar a descrição do produto.</p>

                    <Link className="back-link" to="/profile">
                        <FiArrowLeft size={24} color="#E02041"/> 
                        Voltar para home
                    </Link>

                </section>


                <form onSubmit={handleNewAnotation} >

                    <input 
                        placeholder= "Nome do produto"
                        value={title}
                        onChange={e => setTitle(e.target.value)}
                    />

                    <textarea 
                        placeholder="Descrição do produto.."
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                    />
                    
                    <input 
                        placeholder="Valor(R$)"
                        value={value}
                        onChange={e => setValue(e.target.value)}
                    />


                    <button className="button" type = "submit"> CADASTRAR</button>

                </form>


            </div>

        </div>
    );
}