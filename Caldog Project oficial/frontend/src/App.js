import React from 'react';


import './Global.css';
//JSX(Javascript XML ) = Quando o html está integrado dentro do javascript

import Routes from './routes';

function App() {

  return (
   <Routes/>
  );
}

export default App;
