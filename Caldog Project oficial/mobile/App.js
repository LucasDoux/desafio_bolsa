import 'intl';//biblioteca de internacionalização
import 'intl/locale-data/jsonp/pt-BR' //importando o idioma pt-br

import React from 'react';

import Routes from './src/routes';

export default function App() {
  return (
    <Routes/>
  );
}


