import {StyleSheet} from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
    container:{
        flex:1,
        paddingHorizontal:24,
        paddingTop: Constants.statusBarHeight + 20, //deu 20px de distancia a partir do padding da status bar
        backgroundColor:'#C4E0E5'
        
    },

    header:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
        
    },

    logoImgHeader:{
        width:214,
        height:50,
        marginLeft:-8
    },

    headerText:{
        fontSize:16,
        color: '#737380'
    },

    headerTextBold:{
        fontWeight:'bold',
        fontSize:16
    },

    title:{
        fontSize:30,
        marginBottom: 16,
        marginTop:48,
        color:'#13131a',
        fontWeight:'bold'
    },

    description:{
        fontSize:18,
        lineHeight:24,
        color:'#737380'
    },

    anotationsList:{
        marginTop:18,
    },

    anotation:{
        padding:24,
        borderRadius:10,
        backgroundColor:'#FFF',
        marginBottom:16,
    },

    anotationProperty:{
        fontSize:16,
        color:'#41414d',
        fontWeight:'bold'
    },

    anotationValue:{
        marginTop:8,
        fontSize:16,
        marginBottom:24,
        color:'#737380',
    },

    detailsButton:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },

    detailsButtonText: {
        color:'#6C63FF',
        fontSize:16,
        fontWeight:'bold',
        
    }
});