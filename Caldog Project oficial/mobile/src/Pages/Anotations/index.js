import React, {useEffect, useState} from 'react';
import {Feather} from '@expo/vector-icons'; //icones
import {useNavigation} from '@react-navigation/native'; //possibilidade e navegar para outra tela
import { View , Image , FlatList , Text , TouchableOpacity } from 'react-native';

import api from '../../Services/api';

import logoImg from '../../Assets/Logo.png';

import styles from './styles';

export default function Anotations(){

    const [anotations, setAnotations] = useState([]);//como vai ser preenchido de informação, o array começa vazio
    const [total, setTotal] = useState(0); //começa com 0 pois vai ser um valor numerico/ armazena o total de itens
    const [page, setPage] = useState(1);//inicia na pagina 1

    const [loading,setLoading] = useState(false);//armazena informação quando estar buscando dados novos

    const navigation = useNavigation(); //constante de navegação



    function navigationToDetail(anotation){

        navigation.navigate('Detail', {anotation}); //passando rota de navegação e enviando as informações para a pagina de detalhes da anotação
    }



    async function loadAnotations(){

        if(loading){
            return;//evitando enquanto outra requisição seja feita , outra venha a acontecer
        }

        if(total > 0 && anotations.length === total){ //total de registros de anotações, se chegou no limite de informações ele n ira buscar mais infromações
            return;
        } 

        setLoading(true);

        const response = await api.get('anotations', { //realizando conexão com a api
            params:{ page }
        });

        
        setAnotations([...anotations,...response.data]);//dados vindos da api(lista de anotações)// anexando os novos valores carregados a mesma pagina
        setTotal(response.headers['x-total-count']) //os totais de anotações vem traves de headers
        setPage(page +1) ;
        setLoading(false);

    }


    useEffect(() =>{ //carregando as informações assim que o componente for exibido em tela
        loadAnotations();
    },[]);

    return(
        <View style={styles.container}>

            <View style={styles.header}>

                <Image source={logoImg} style={styles.logoImgHeader}/>

                <Text style={styles.headerText}>

                    <Text style={styles.headerTextBold}> {total} Anotações </Text>   

                </Text>

            </View>


            <Text style={styles.title}>Bem Vindo!</Text>
            <Text style={styles.description}>Suas Anotações Abaixo:</Text>


            <FlatList
                data={anotations}
                style={styles.anotationsList}
                keyExtractor={anotation => String(anotation.id) }
                showsVerticalScrollIndicator={false}
                onEndReached={loadAnotations} //quando o usuario chega no final da lista ele executa a função criada de loadAnotations
                onEndReachedThreshold={0.2} //se estiver 20% do final da lista ele carrega mais itens
                renderItem={({ item: anotation }) => (

                    <View style={styles.anotation}>

                        <Text style={styles.anotationProperty}> USER: </Text>
                        <Text style={styles.anotationValue}> {anotation.name} </Text>

                        <Text style={styles.anotationProperty}> PRODUTO: </Text>
                        <Text style={styles.anotationValue}> {anotation.title} </Text>

                        <Text style={styles.anotationProperty}> VALOR: </Text>
                        <Text style={styles.anotationValue}> 
                            {Intl.NumberFormat('pt-BR', { 
                                style: 'currency' , 
                                currency:'BRL'
                                }).format(anotation.value)} 
                        </Text>

                        <TouchableOpacity 
                        style={styles.detailsButton}
                        onPress={ () => navigationToDetail(anotation) }
                        >
                            <Text style={styles.detailsButtonText}>Ver mais detalhes</Text>

                            <Feather name="arrow-right" size={28} color='#6C63FF'/>

                         </TouchableOpacity>

                    </View>

                )}
            />
            
            
        </View>
    );
}