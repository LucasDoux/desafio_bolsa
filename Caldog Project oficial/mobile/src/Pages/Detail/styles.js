import {StyleSheet} from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
    container:{
        flex:1,
        paddingHorizontal:24,
        paddingTop: Constants.statusBarHeight + 20,
        backgroundColor:'#C4E0E5'
    },
    
    logoImgHeader:{
        width:190,
        height:46,
        marginLeft:-8
    },

    header:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center'
    },

    anotation:{
        padding:24,
        borderRadius:10,
        backgroundColor:'#FFF',
        marginBottom:16,
        marginTop:48,
    },

    anotationProperty:{
        fontSize:16,
        color:'#41414d',
        fontWeight:'bold',
        marginTop:24
    },

    anotationValue:{
        marginTop:8,
        fontSize:16,
        color:'#737380',
    },

    logoImgDetail:{
        width:120,
        height:120,
        alignSelf:"center",
        marginTop:45
    }
})