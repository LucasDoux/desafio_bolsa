import React from 'react';
import {Feather} from '@expo/vector-icons'; //icones
import {useNavigation, useRoute} from '@react-navigation/native';//useRoute pega informações especificas da pagina atual da aplicação
import { View , Image , FlatList , Text , TouchableOpacity } from 'react-native';

import logoImg from '../../Assets/Logo.png';

import imgDetail from '../../Assets/dog.png'

import styles from './styles';

export default function Detail(){

    const navigation = useNavigation();
    const route = useRoute();

    const anotation = route.params.anotation

    function navigateBack(){
        navigation.goBack('Anotations');
    }
    return(
        <View style={styles.container}>

                <View style={styles.header}>

                    <Image source={logoImg} style={styles.logoImgHeader}/>

                    <TouchableOpacity onPress={navigateBack}>

                        <Feather name="arrow-left" size={28} color="#6C63FF"/>

                    </TouchableOpacity>

                </View>    

                <View style={styles.anotation}>

                        <Text style={[styles.anotationProperty,{marginTop:0}]}> USER: </Text>
                        <Text style={styles.anotationValue}> {anotation.name} </Text>

                        <Text style={styles.anotationProperty}> CACHORRO(NOME): </Text>
                        <Text style={styles.anotationValue}> {anotation.dog} </Text>

                        <Text style={styles.anotationProperty}> PRODUTO: </Text>
                        <Text style={styles.anotationValue}> {anotation.title} </Text>

                        <Text style={styles.anotationProperty}> VALOR: </Text>
                        <Text style={styles.anotationValue}> 
                            {Intl.NumberFormat('pt-BR', { 
                                style: 'currency' , 
                                currency:'BRL'
                                }).format(anotation.value)} 
                        </Text>

                        
                

                 </View>

                 <Image source={imgDetail} style={styles.logoImgDetail}/>
                 
            </View>
    );
}