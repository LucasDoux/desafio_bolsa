import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Anotations from './Pages/Anotations'
import Detail from './Pages/Detail'

const AppStack = createStackNavigator(); // onde vai ser cadastrada as rotas

export default function Routes(){
    return(

      <NavigationContainer>

          <AppStack.Navigator screenOptions={{headerShown:false}}>

            <AppStack.Screen name="Anotations" component={Anotations}/>
            
            <AppStack.Screen name="Detail" component={Detail}/>

          </AppStack.Navigator>

      </NavigationContainer>  

    );
}